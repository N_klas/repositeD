<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use Illuminate\Support\Facades\File;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class PackageController extends Controller
{
    public function update(Request $request, $id) {
        try {
            $requestBody = json_decode($request->getContent(), true);
            $package = Package::where('id', '=', $id)->get()[0];
            //dd($requestBody);
            if($requestBody["event_type"] == "merge_request") {
                if($requestBody["object_attributes"]["state"] == "merged") {
                    $client = new Client();
                    $res = $client->get($package->rawBuildFile);
                    echo($package->rawBuildFile);
                    if($res->getStatusCode() == 200){
                        $buildFile = json_decode($res->getBody(), true);
                        $package->name = $buildFile["project"]["name"];
                        $package->author = $buildFile["project"]["author"];
                        $package->license = $buildFile["project"]["license"];
                        $package->version = $buildFile["project"]["version"];
                        
                        $package->save();
                    } else {
                        abort(400, "Could not successfully fetch BuildFile! " . $res->getStatusCode());
                    }
                }
            } else {
                die("This is not an interessting event");
            }
        } catch (Exception $ex) {
            //abort(404, $ex);
        }
    }

    public function add(Request $request){

    }

}
